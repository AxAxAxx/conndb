# conndb

## Push container To Google Cloud
1. docker build -t asia.gcr.io/ics-research/uat-garage-intodb . <-- Build in Docker
2. docker push asia.gcr.io/ics-research/uat-garage-intodb <-- Push to GoogleCloud

## Migration DB
- migrate create -ext sql -dir db/migration -seq create_table <-- migrate created file sql
- migrate up
    - migrate -database 'postgres://username:password@host:port/databasename?sslmode=disable' -path db/migration up
- migrate down 
    - migrate -database 'postgres://username:password@host:port/databasename?sslmode=disable' -path db/migration down