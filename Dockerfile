FROM golang:1.21-alpine AS builder

WORKDIR /app

COPY go.* ./

RUN go mod download

COPY . ./

RUN go build -o main ./main.go

FROM alpine:latest

ENV TZ=Asia/Bangkok

WORKDIR /app

COPY --from=builder /app .

ENV GOOGLE_APPLICATION_CREDENTIALS=/app/application_default_credentials.json

EXPOSE 3000

CMD [ "./main" ]
