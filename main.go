package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

type CsvLine struct {
	GarageType  string
	GarageName  string
	OwnerName   string
	Telephone   string
	Fax         string
	Email       string
	Address     string
	Region      string
	Insurer     string
	Province    string
	District    string
	SubDistrict string
}

type Configs struct {
	PostgreSQL PostgreSQL
	Server     Server
}

type Server struct {
	Host string
	Port string
}

type PostgreSQL struct {
	Host     string
	Port     string
	Protocol string
	Username string
	Password string
	Database string
	SSLMode  string
}

func newConfig() *Configs {
	if err := godotenv.Load(".env"); err != nil {
		panic(err.Error())
	}

	return &Configs{
		Server: Server{
			Host: os.Getenv("SERVER_HOST"),
			Port: os.Getenv("SERVER_PORT"),
		},
		PostgreSQL: PostgreSQL{
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			Username: os.Getenv("DB_USERNAME"),
			//Password: os.Getenv("DB_PASSWORD"),
			Password: `2Fc$E@ky"<=kB>{O`,
			Database: os.Getenv("DB_DATABASE"),
			SSLMode:  os.Getenv("DB_SSL_MODE"),
		},
	}
}

func ConnPgSQL(cfg *Configs) (*sqlx.DB, error) {
	connpostgre := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		cfg.PostgreSQL.Host, cfg.PostgreSQL.Port, cfg.PostgreSQL.Username, cfg.PostgreSQL.Password, cfg.PostgreSQL.Database, cfg.PostgreSQL.SSLMode)

	db, err := sqlx.Connect("postgres", connpostgre)
	if err != nil {
		return nil, err
	}
	fmt.Println("-Connect-")
	return db, nil
}

func getClient(ctx context.Context) (*storage.Client, error) {
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func readCSVFromGCS(ctx context.Context, bucketName, objectName string) ([]string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, error) {
	client, err := getClient(ctx)
	if err != nil {
		return nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, err
	}
	rc, err := client.Bucket(bucketName).Object(objectName).NewReader(ctx)
	if err != nil {
		return nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, err
	}
	defer rc.Close()

	reader := csv.NewReader(rc)
	_, err = reader.Read()
	if err != nil && err != io.EOF {
		return nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, err
	}

	records, err := reader.ReadAll()
	if err != nil {
		return nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, err
	}

	var garagetype, garagename, ownername, telephone, fax, email, address, region, insurer, province, district, sub_district []string
	for _, line := range records {
		data := CsvLine{
			GarageType:  line[0],
			GarageName:  line[1],
			OwnerName:   line[2],
			Telephone:   line[3],
			Fax:         line[4],
			Email:       line[5],
			Address:     line[6],
			Region:      line[7],
			Insurer:     line[8],
			Province:    line[9],
			District:    line[10],
			SubDistrict: line[11],
		}
		garagetype = append(garagetype, data.GarageType)
		garagename = append(garagename, data.GarageName)
		ownername = append(ownername, data.OwnerName)
		telephone = append(telephone, data.Telephone)
		fax = append(fax, data.Fax)
		email = append(email, data.Email)
		address = append(address, data.Address)
		region = append(region, data.Region)
		insurer = append(insurer, data.Insurer)
		province = append(province, data.Province)
		district = append(district, data.District)
		sub_district = append(sub_district, data.SubDistrict)
	}
	return garagetype, garagename, ownername, telephone, fax, email, address, region, insurer, province, district, sub_district, nil
}

func AddinfoSQL(table string, data []string, db *sqlx.DB) error {
	for _, v := range data {
		query := fmt.Sprintf("INSERT INTO public.%s(name)SELECT ('%s') WHERE NOT EXISTS (SELECT 1 FROM %s WHERE name='%s');\n", table, v, table, v)
		_, err := db.Exec(query)
		if err != nil {
			return err
		}
	}
	return nil
}

func DisableStatus(table string, db *sqlx.DB) error {
	updateQuery := fmt.Sprintf("UPDATE %s SET status = 'Disable';", table)
	_, err := db.Exec(updateQuery)
	if err != nil {
		return err
	}
	return nil
}

func GarageSQL(table string, garagetype, garagename, ownername, telephone, fax, email, address, region, insurer, province, district, sub_district []string, db *sqlx.DB) error {
	for i := range garagetype {
		query := fmt.Sprintf(`INSERT INTO %s (
			garage_name, owner_name, telephone, fax, email, status, garage_type_id, insurer_id, address_details, sub_district_id, district_id, province_id, region_id)
		SELECT
			('%s'),
			('%s'),
			('%s'),
			('%s'),
			('%s'),
			('Activate'),
			(select id from garage_type WHERE name = '%s'),
			(select id from insurer WHERE name = '%s'),
			('%s'),
			(select id from sub_district WHERE name = '%s'),
			(select id from district WHERE name = '%s'),
			(select id from province WHERE name = '%s'),
			(select id from region WHERE name = '%s')
		WHERE NOT EXISTS (
			SELECT * FROM garage WHERE garage_name='%s'
								AND owner_name = '%s'
								AND insurer_id= (select id from insurer WHERE name = '%s')
								AND address_details='%s'
								AND garage_type_id = (select id from garage_type WHERE name = '%s'));`+"\n",
			table, garagename[i], ownername[i], telephone[i], fax[i],
			email[i], garagetype[i], insurer[i], address[i], sub_district[i],
			district[i], province[i], region[i], garagename[i], ownername[i],
			insurer[i], address[i], garagetype[i])
		_, err := db.DB.Exec(query)
		if err != nil {
			return err
		}
	}
	return nil
}

func ActivateStatus(garagename, ownername, insurer, address, garagetype []string, db *sqlx.DB) error {
	updateStatusQuery := `UPDATE garage
                          SET status = 'Activate'
                          WHERE status = 'Disable'
                          AND garage_name = $1
                          AND owner_name = $2
                          AND insurer_id = (SELECT id FROM insurer WHERE name = $3)
                          AND address_details = $4
                          AND garage_type_id = (SELECT id FROM garage_type WHERE name = $5);`

	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	stmt, err := tx.Prepare(updateStatusQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	for i := range garagename {
		_, err := stmt.Exec(garagename[i], ownername[i], insurer[i], address[i], garagetype[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func processData(c *fiber.Ctx) error {
	ctx := context.Background()
	bucketName := "asia.artifacts.ics-research.appspot.com"
	objectName := []string{
		"data_garage/viriyah/viriyah_garage.csv",
		"data_garage/thanachart/thanachart_garage.csv",
		"data_garage/muangthai/muangthai_center_garage.csv",
		"data_garage/muangthai/muangthai_garage.csv",
		"data_garage/chubb/chubb_garage.csv",
		"data_garage/bangkok/bangkok_center_garage.csv",
		"data_garage/bangkok/bangkok_garage.csv",
	}
	list_table := []string{"garage_type", "insurer", "region", "province", "district", "sub_district", "garage"}

	cgf := newConfig()

	db, err := ConnPgSQL(cgf)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	for _, v := range objectName {
		garagetype, garagename, ownername, telephone, fax, email, address, region, insurer, province, district, sub_district, err := readCSVFromGCS(ctx, bucketName, v)
		if err != nil {
			log.Fatalf("Error reading CSV from GCS: %v", err)
		}
		for i, table := range list_table {
			var data []string
			switch i {
			case 0:
				data = garagetype
			case 1:
				data = insurer
			case 2:
				data = region
			case 3:
				data = province
			case 4:
				data = district
			case 5:
				data = sub_district
			}

			err = AddinfoSQL(table, data, db)
			if err != nil {
				log.Printf("Error adding data to table %s: %v", table, err)
				break
			}
		}
		err = DisableStatus(list_table[6], db)
		if err != nil {
			panic(err)
		}
		err = GarageSQL(list_table[6], garagetype, garagename, ownername, telephone, fax, email, address, region, insurer, province, district, sub_district, db)
		if err != nil {
			panic(err)
		}
		defer ActivateStatus(garagename, ownername, insurer, address, garagetype, db)
	}
	return c.SendString("Data processed successfully")
}

func main() {
	app := fiber.New()
	app.Get("/", processData)

	cfg := newConfig()
	connectionURL := fmt.Sprintf("%s:%s", cfg.Server.Host, cfg.Server.Port)
	log.Fatal(app.Listen(connectionURL))
}
