-- Drop foreign key constraints
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_garage_type_id_fkey;
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_insurer_id_fkey;
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_sub_district_id_fkey;
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_district_id_fkey;
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_province_id_fkey;
ALTER TABLE "garage" DROP CONSTRAINT IF EXISTS garage_region_id_fkey;

-- Drop tables
DROP TABLE IF EXISTS "garage";
DROP TABLE IF EXISTS "garage_type";
DROP TABLE IF EXISTS "sub_district";
DROP TABLE IF EXISTS "district";
DROP TABLE IF EXISTS "province";
DROP TABLE IF EXISTS "region";
DROP TABLE IF EXISTS "insurer";
