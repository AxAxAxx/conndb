CREATE TABLE "garage" (
  "id" SERIAL PRIMARY KEY,
  "garage_name" text,
  "owner_name" text,
  "telephone" text,
  "fax" text,
  "email" text,
  "status" text,
  "garage_type_id" int,
  "insurer_id" int,
  "address_details" text,
  "sub_district_id" int,
  "district_id" int,
  "province_id" int,
  "region_id" int
);

CREATE TABLE "garage_type" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

CREATE TABLE "sub_district" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

CREATE TABLE "district" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

CREATE TABLE "province" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

CREATE TABLE "region" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

CREATE TABLE "insurer" (
  "id" SERIAL PRIMARY KEY,
  "name" text
);

ALTER TABLE "garage" ADD FOREIGN KEY ("garage_type_id") REFERENCES "garage_type" ("id");

ALTER TABLE "garage" ADD FOREIGN KEY ("insurer_id") REFERENCES "insurer" ("id");

ALTER TABLE "garage" ADD FOREIGN KEY ("sub_district_id") REFERENCES "sub_district" ("id");

ALTER TABLE "garage" ADD FOREIGN KEY ("district_id") REFERENCES "district" ("id");

ALTER TABLE "garage" ADD FOREIGN KEY ("province_id") REFERENCES "province" ("id");

ALTER TABLE "garage" ADD FOREIGN KEY ("region_id") REFERENCES "region" ("id");
